const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    // Read content from the storage.txt file asynchronously
    fs.readFile('storage.txt', 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
        } else {
            // Render the homepage with the form and all stored data
            res.send(renderHomepage(data || 'No data stored yet.'));
        }
    });
});

app.post('/submit', (req, res) => {
    const data = req.body.data;

    // Append the data to the storage.txt file
    fs.appendFile('storage.txt', `${data}\n`, (err) => {
        if (err) {
            console.error(err);
            res.status(500).send('Internal Server Error');
        } else {
            // Read content from the storage.txt file asynchronously
            fs.readFile('storage.txt', 'utf8', (readErr, readData) => {
                if (readErr) {
                    console.error(readErr);
                    res.status(500).send('Internal Server Error');
                } else {
                    // Render the updated homepage with the form and all stored data
                    res.send(renderHomepage(readData || 'No data stored yet.'));
                }
            });
        }
    });
});

// Function to render the homepage with the form and all stored data
function renderHomepage(allData) {
    const dataArray = allData.split('\n').filter(Boolean);

    const dataItems = dataArray.map((item, index) => `<p key=${index}>${item}</p>`).join('');
    return `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Node.js Web App</title>
        </head>
        <body>
            <h1>Welcome to the Node.js Web App!</h1>
            <form action="/submit" method="post">
                <label for="data">Enter data:</label>
                <input type="text" id="data" name="data" required>
                <button type="submit">Submit</button>
            </form>

            <h2>All Stored Data:</h2>
            <p>${allData}</p>
        </body>
        </html>
    `;
}

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
